import {environment} from "../environment";
import {console} from "next/dist/compiled/@edge-runtime/primitives/console";

     export async function loadNamespaces() {

        const response = await fetch( "http://127.0.0.1:8080/api/v1/namespaces", {method: 'GET'}, {
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Methods': 'GET,POST,DELETE,PUT',
                'Authorization': 'Bearer ' + environment.token
            }
        })
            .then((res) => res.json(), console.log(environment.token))
            .then((data) => {
                console.log(data);
            });

        const json = await response.json();
        return {
            items: json.results,
        };
    }
