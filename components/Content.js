import {Text, Input} from "@nextui-org/react"
import { Box } from "./Box.js"
import {environment} from "../environment";
import {loadNamespaces} from "../services/NamespacesService";
export const Content = () => (

    <Box css={{px: "$12", mt: "$8", "@xsMax": {px: "$10"}}}>
        <Text h2>Your token</Text>
        <Input  aria-label="Insert Token" id="token" type="text" placeholder="Insert your token here" />
        <button type="submit" onClick={submitToken}>Submit</button>
        <button onClick={loadNamespaces}> Fetch</button>
    </Box>

);

function submitToken(){
    let key = document.getElementById('token').value;
    environment.token = key;
}

