import { Table, useAsyncList, useCollator } from "@nextui-org/react";
import {environment} from "../environment";
import {console} from "next/dist/compiled/@edge-runtime/primitives/console";

export default function ResourceTable() {

    const collator = useCollator({ numeric: true });

    async function load() {
        console.log(environment.token);
        const res = await fetch("/apis/mast.ansi.services/v1/namespaces/default/mastjobs", {
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Methods': 'GET,POST,DELETE,PUT',
                'Authorization': 'Bearer ' + environment.token
            }
        });
        const json = await res.json();
        console.log(json);
        return {
            items: json.results,
        };
    }
    async function sort({ items, sortDescriptor }) {
        return {
            items: items.sort((a, b) => {
                let first = a[sortDescriptor.column];
                let second = b[sortDescriptor.column];
                let cmp = collator.compare(first, second);
                if (sortDescriptor.direction === "descending") {
                    cmp *= -1;
                }
                return cmp;
            }),
        };
    }
    const list = useAsyncList({ load, sort });
    return (
        <Table
            aria-label="Example static collection table"
            css={{ minWidth: "100%", height: "calc($space$14 * 10)" }}
            sortDescriptor={list.sortDescriptor}
            onSortChange={list.sort}
        >
            <Table.Header>
                <Table.Column key="name" allowsSorting>
                    Name
                </Table.Column>
                <Table.Column key="height" allowsSorting>
                    Age
                </Table.Column>
            </Table.Header>
            <Table.Body items={list.items} loadingState={list.loadingState}>
                {(item) => (
                    <Table.Row key={item.name}>
                        {(columnKey) => <Table.Cell>{item[columnKey]}</Table.Cell>}
                    </Table.Row>
                )}
            </Table.Body>
        </Table>
    );
}
