export interface Root {
    kind: string;
    apiVersion: string;
    metadata: Metadata;
    items: Item[];
}

export interface Metadata {
    resourceVersion: string;
}

export interface Item {
    metadata: Metadata2;
    spec: Spec;
    status: Status;
}

export interface Metadata2 {
    name: string;
    uid: string;
    resourceVersion: string;
    creationTimestamp: string;
    labels: Labels;
    managedFields: ManagedField[];
}

export interface Labels {
    'kubernetes.io/metadata.name': string;
}

export interface ManagedField {
    manager: string;
    operation: string;
    apiVersion: string;
    time: string;
    fieldsType: string;
}

export interface Spec {
    components: Components[];
    finalizers: string[];
}

export interface Status {
    phase: string;
}
export interface Components {
    type: string;
}
