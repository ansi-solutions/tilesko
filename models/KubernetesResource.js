export class KubernetesResource {
    apiVersion: string;
    kind: string;
    metadata: Object;
    spec: Object;
}
