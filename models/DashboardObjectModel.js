export interface DashboardObject {
    name: string;
    age: string;
}
export interface Dashboards {
    dashboards: DashboardObject[];
}
